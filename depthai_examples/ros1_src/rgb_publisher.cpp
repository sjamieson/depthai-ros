
#include "ros/ros.h"

#include <iostream>
#include <cstdio>
// #include "utility.hpp"
#include "sensor_msgs/Image.h"
#include "std_msgs/ByteMultiArray.h"
#include <camera_info_manager/camera_info_manager.h>

// Inludes common necessary includes for development using depthai library
#include "depthai/depthai.hpp"
#include <depthai_bridge/BridgePublisher.hpp>
#include <depthai_bridge/ImageConverter.hpp>

dai::Pipeline createPipeline(){
    dai::Pipeline pipeline;
    auto colorCam = pipeline.create<dai::node::ColorCamera>();
    auto xlinkRgbOut = pipeline.create<dai::node::XLinkOut>();
    auto xlinkRawOut = pipeline.create<dai::node::XLinkOut>();
    xlinkRgbOut->setStreamName("video");
    xlinkRawOut->setStreamName("raw");
    
    colorCam->setPreviewSize(300, 300);
    colorCam->setResolution(dai::ColorCameraProperties::SensorResolution::THE_1080_P);
    colorCam->setInterleaved(false);

    // Link plugins CAM -> XLINK
    colorCam->video.link(xlinkRgbOut->input);
    colorCam->raw.link(xlinkRawOut->input);

    return pipeline;
}

int main(int argc, char** argv){

    ros::init(argc, argv, "rgb_node");
    ros::NodeHandle pnh("~");
    
    std::string tfPrefix;
    std::string camera_param_uri;
    int badParams = 0;

    badParams += !pnh.getParam("tf_prefix", tfPrefix);
    badParams += !pnh.getParam("camera_param_uri", camera_param_uri);

    if (badParams > 0)
    {
        throw std::runtime_error("Couldn't find one of the parameters");
    }
    
    dai::Pipeline pipeline = createPipeline();
    dai::Device device(pipeline);
    std::shared_ptr<dai::DataOutputQueue> imgQueue = device.getOutputQueue("video", 30, false);
    std::shared_ptr<dai::DataOutputQueue> rawQueue = device.getOutputQueue("raw", 30, false);
    
    std::string color_uri = camera_param_uri + "/" + "color.yaml";

    dai::rosBridge::ImageConverter rgbConverter(tfPrefix + "_rgb_camera_optical_frame", false);
    dai::rosBridge::BridgePublisher<sensor_msgs::Image, dai::ImgFrame> rgbPublish(imgQueue,
                                                                                  pnh, 
                                                                                  std::string("color/image"),
                                                                                  std::bind(&dai::rosBridge::ImageConverter::toRosMsg, 
                                                                                  &rgbConverter, // since the converter has the same frame name
                                                                                                  // and image type is also same we can reuse it
                                                                                  std::placeholders::_1, 
                                                                                  std::placeholders::_2) , 
                                                                                  30,
                                                                                  color_uri,
                                                                                  "color");

    rgbPublish.addPublisherCallback();

    ros::Publisher rawPublisher = pnh.advertise<std_msgs::ByteMultiArray>("raw/image", 30);
    auto callback = [&](ros::TimerEvent const& event) -> void {
        auto data = rawQueue->tryGet<dai::ImgFrame>();
        if (!data) return;
        auto mat = data->getCvFrame();
        if (mat.type() != CV_8UC1) throw std::runtime_error("Unexpected matrix type " + std::to_string(mat.type()));
        std_msgs::ByteMultiArray msg;
        msg.data = mat.isContinuous() ? mat : mat.clone();
        msg.layout.dim.resize(1);
        msg.layout.dim[0].label = "RAW10";
        msg.layout.dim[0].size = mat.total();
        msg.layout.dim[0].stride = mat.total();
        rawPublisher.publish(msg);
    };
    ros::Timer timer = pnh.createTimer(ros::Duration(0.2), callback);

    ros::spin();

    return 0;
}

